# EM Shadow Program

The EM Shadow Program is an unstructured program for team members to shadow an Engineering Manager. 

## Goal

- To give future managers and overview on how the day to day looks like for an Engineering Manager.
- To provide the opportunity for take a hands-on role on some EM activities

## Format

As much unstructured as possible!

- Sync 30 minutes slots on Mondays at 15:00 UTC and Wednesdays at 6:30 UTC. [Zoom link](https://gitlab.zoom.us/j/97563413541?pwd=VEhZTG9nYzZ6RThXWDUycVJybVAwUT09)
- Issues to track activities, questions and notes

## How to apply

Just come and join the call!
