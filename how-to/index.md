## How To Series

This is a series of different markdown pages with steps on how to do some EM tasks in full Michelle's style.
Please note that this goes totally against my philosophy, but, 🙏🏽 I hope it will be helpful for the team members covering for me during my time off.

😤 I don't like this  

I always have problems when I'm asked to share a list of steps to complete a task. In my manager's philosophy, this removes the autonomy and creativity of the person that will execute a task. I prefer to describe what we achieve and why it is important, so the person in charge can decide the best way to do things and let their creativity and judgment play in the game.
