# How to monitor customer escalations, request for help, and slack channels

## My process

1. I'm [subscribed to multiple labels](https://docs.gitlab.com/ee/user/project/labels.html#receive-notifications-when-a-label-is-used).
    1. In [gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com) I'm subscribed to: `Help group::container registry`, `devops::package` and `group::container registry`
    1. In [gitlab-org](https://gitlab.com/gitlab-org/gitlab/-/labels) I'm subscribed to: `devops::package` and `group::container registry`
1. I monitor [slack with keywords](https://slack.com/help/articles/201355156-Configure-your-Slack-notifications) related to my team topics 
```
container registry, package registry, package_registry, container_registry, container-registry, package-registry, dependency proxy, clean up policies, clean-up policies, cleanup policies, gitlab registry
```
1. I check my email deeply at least once per week. The subscription to labels gives me the opportunity to be aware of topics on my email where I was not pinged but may require my attention.
1. I review slack multiple times a day. The slack notifications allow me to be aware of how other teams may be using our product or if they need help with an specific area.
1. Note that my action to those notifications are mostly about planning to schedule issues in the future, answer to users asking for more information, ping the team if I see something is important and I do not have the answer or ask on slack to create an issue so we can follow up.
