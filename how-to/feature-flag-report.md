# How to for Feature Flags Reporting

## Useful links
- 

## My process

### Monthly report

There is a monthly error budget report created in https://gitlab.com/gitlab-org/quality/triage-reports/-/issues, example https://gitlab.com/gitlab-org/quality/triage-reports/-/issues/9323

1. Review critically the list of features flags, think about what can be removed, what cannot and why
1. Add a comment on the status
1. Share on milestone planning or wherever see fit about feature flags that should be scheduled for removal
