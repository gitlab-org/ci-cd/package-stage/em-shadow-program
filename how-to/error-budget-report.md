# How to for Error Budget Reporting

## Useful links
- [What are error budgets](https://about.gitlab.com/handbook/engineering/error-budgets/)
- [Video walkthrough to review error budget spikes](https://youtu.be/ocvps-AaFNE)

## My process

### Weekly report

Every week there is a bot on slack asking for anyone to report on the group's error budget for the past 7 days

1. Click on the link for the error budget dashboard
1. Click on the button Yes
1. Review critically the error budget on the dashboard. Is it red, is it green, do budget looks ok or is at risk to get red?
1. In slack, as a reponse to the Slackbot message, answer the questions. I add a 🟢, 🔴 or 🥵 depending on the numbers for readability
1. If the budget is red or close to red
   1. investigate quickly what could be the reason
   1. Open an issue and label it as Error Budget Improvement
   1. On the response that is autoposted on the group channel start on a thread with the issue to follow up. 
   1. Ping PM so we can decide if that could be prioritized in the current or next milestone depending on impact and risk
   1. Make sure SrEM or even Director are aware so they can communicate up on the ladder
1. Re-share the post in #ops-section

### Monthly report

There is a monthly error budget report created in https://gitlab.com/gitlab-org/error-budget-reports/-/issues

1. Find my group in the table
1. Edit the issue and fill the table with the information required
1. If the budget is red
   1. Follow the same critical thinking and judgment mentioned in the monthly
   1. Confirm there are issues to fix it and are labeled as Error Budget Improvement
   1. Add any notes on current status, reason of the problem, next steps etc
   1. Make sure SrEM or even Director are aware so they can communicate up on the ladder
1. Add a comment as a message to confirm things were reviewed for the group
