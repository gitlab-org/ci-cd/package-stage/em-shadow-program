# How to for Release Posts

## Useful links

- https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Slack channel #release-post
- Search urls for each type:
  - Bugs: https://gitlab.com/dashboard/issues?scope=all&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=type%3A%3Abug&milestone_title=15.6
  - Peformance improvements: https://gitlab.com/dashboard/issues?scope=all&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=bug%3A%3Aperformance&milestone_title=15.6
  - Usability improvements: https://gitlab.com/dashboard/issues?scope=all&state=opened&label_name[]=group%3A%3Acontainer%20registry&label_name[]=bug%3A%3Aux&milestone_title=15.6

## My process

Every month, new MRs are created to contribute with list Usability improvements, Bugs, or Performance improvements that groups deliver in the milestone.

1. Find the MRs with a search by `release-XX-X`, example https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests?scope=all&state=opened&search=release-15-6
1. Review the list of bugs the group delivered in the milestone, example https://gitlab.com/dashboard/issues?scope=all&state=all&label_name[]=group%3A%3Acontainer%20registry&label_name[]=type%3A%3Abug&milestone_title=15.6
    1. ⚠️ A judgment is required to decide if an issue should be listed as bug, performance improvement or usability improvement, in the end, all those are ~type::bug
1. If there are no issues to be listed, add a comment like this https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/114279#note_1165376495
1. For the closed issues, decide if they should be part of the release post. Normally yes, unless is something not user facing, the issue was closed without fixing, was promoted to epic, or similar scenarios.
1. For open issues, if there is something that I'm confident it will make it into the milestone, add it
1. If the issue titles are not clear, reword them in the MR to make easier understading what was the bug fixed
1. Add a comment on the issues to notify those were included in the post, example https://gitlab.com/gitlab-org/gitlab/-/issues/361279#note_1165328741
1. For issues that are still open, ping in the comment the DRI so they are aware that the issue was included if this doesnt make the milestone then they should remove it from the MR, example https://gitlab.com/gitlab-org/container-registry/-/issues/815#note_1165335052
1. Commit changes
1. In the MR, open a review pinging the Product Manager and Tech Writer for review, example https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/114277/diffs#note_1165348806
    1. For usability improvements also ping the Product Designer
1. Add a comment on the MR to confirm the work is completed, example https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/114277#note_1165350779



